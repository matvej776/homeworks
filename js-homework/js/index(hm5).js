let createNewUser = () => {
    let firstName;
    let lastName
  do {
      firstName = prompt("What is your name?")
  }while (!isNaN(firstName) || firstName === null )
    do {
        lastName = prompt("What is your surname?")
    }while (!isNaN(lastName) || lastName === null )

    let userInfo = {
        firstName: firstName,
        lastName: lastName,
        getLogin: function() {
            const firstInitial = this.firstName.charAt(0).toLowerCase();
            const lastNameLower = this.lastName.toLowerCase();
            return firstInitial + lastNameLower;
        }

    };
    return userInfo;
}
const newUser = createNewUser();

console.log(newUser.getLogin())

