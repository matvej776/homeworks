

// 1) Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.
//
// 2) Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.
//
// 3) Запитайте у користувача якесь значення і виведіть його в консоль.

// 1 )
let myName = "matthew";
let admin ;
admin=myName;
console.log(admin);

// 2 )
let days = 2;
console.log(days*24*60*60);

// let days = 2;
// let seconds;                                    ВТОРОЙ ВАРИАНТ
// seconds=days*24*60*60;
// console.log(seconds)

// 3)
let userName = prompt("What's your name?");
console.log(userName);